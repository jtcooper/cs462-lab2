ruleset twilio {
  meta {
    configure using account_side = ""
            auth_token = ""
    provides
      send_sms,
      messages,
      next_message_results
  }
  global {
    base_url = <<https://#{account_sid}:#{auth_token}@api.twilio.com>>
    uri = <</2010-04-01/Accounts/#{account_sid}/Messages.json>>
    
    send_sms = defaction(to, from, message) {
	  		http:post(base_url + uri, form =
		                {"From":from,
		                 "To":to,
		                 "Body":message
		                },
		                autoraise="messages")  // autoraise "messages" should be handled by the caller
		}
		
		messages = function(to = null, from = null, page_size = 50) {
		  params = {"To": to,
  		    "From": from,
  		    "PageSize": page_size};
		  http:get(base_url + uri, qs=params.filter(function(v,k) {v}), autoraise="messages")
		}
		
		next_message_results = defaction(next_page) {
  		  http:get(base_url + next_page,
  		    autoraise="messages")
		}
  }
}

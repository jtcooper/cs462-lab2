ruleset lab2 {
	meta {
		name "Lab 2"
	    description <<
			Ruleset for Lab 2 using Twilio API
		>>
		author "Josh Cooper"
		use module lab2_keys
    use module twilio
        with account_sid = keys:twilio{"account_sid"}
             auth_token =  keys:twilio{"auth_token"}
	}

	rule test_send_sms {
	    select when test new_message
	    pre {
	      to = event:attr("to").klog("to value: ")
	      from = event:attr("from").klog("from value: ")
	      message = event:attr("message").klog("message value: ")
	    }
	   twilio:send_sms(event:attr("to"),
	           event:attr("from"),
	           event:attr("message"))
	}
	
	rule request_messages {
	  select when test messages where (event:attr("next_page") == null)
	  // sender: sender number (default: none)
	  // recipient: recipient number (default: none)
	  // page_size: num results per page (default: 50)
	  pre {
	    b = klog("request_messages endpoint received request")
	  }
	  fired {
      twilio:messages(event:attr("to"),
        event:attr("from"),
        event:attr("page_size"))
	  }
	}
	
	rule request_next_page {
	  select when test messages where event:attr("next_page")
	  // next_page: next page URI (trumps all other parameters)
	  pre {
	    b = klog("request_next_page endpoint received request")
	  }
    twilio:next_message_results(event:attr("next_page"))
	}
	
	rule new_message_response {
	  select when http post label re#messages#
	  pre {
	    b = klog("new_message_response fired")
	  }
	  send_directive("content", {"response":event:attr("content").decode()})
	}
	
	rule process_messages {
	  select when http get label re#messages#
	  pre {
	    b = klog("process_messages fired")
	  }
	  send_directive("content", {"messages":event:attr("content").decode()})
	}
}